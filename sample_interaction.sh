#!/bin/sh
# sample interaction with remate calculator server

# last two commands won't work
BOB_INPUT="login bob
password bob
calc 0
calc 2 + 3
calc 2+2*2
calc 3 / 3 * 3
calc 3x^2
oops"


if [ ! $1 ]
then 
  echo "What port?";
  exit 1;
fi


echo "$BOB_INPUT" | nc 127.0.0.1 $1

