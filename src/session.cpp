// TCP session with 1 user
// shamelessly stolen from:
// [here](https://dens.website/tutorials/cpp-asio/async-tcp-server)


#include <iostream>
#include <optional>
#include <boost/asio.hpp>
#include "srv_commands.cpp"


// longest safely accepted response 
#define RESPONSE_MAX_LEN 128

using namespace boost::asio;

class session : public std::enable_shared_from_this<session>
{
public:

    session(ip::tcp::socket&& socket)
    : socket(std::move(socket))
    {
    }

    // exchange data with client over TCP
    // incoming messages are expected to end with \n
    // and be shorter then RESPONSE_MAX_LEN
    void start()
    {
        sendMessage("Hello!\n>");
    }

private:


    void sendMessage(const std::string message)
    {
        async_write(socket, buffer(message),
            [self = shared_from_this()] (boost::system::error_code error,
                                         std::size_t bytes_transferred)
            {
                // TODO: write errors
                self->getResponse();
            });
    }


    void getResponse()
    {
        async_read_until(socket, input_buf, '\n',
          [self = shared_from_this()] (boost::system::error_code error,
                                       std::size_t bytes_transferred)
          {
              // TODO: read errors
              std::istream input_stream (&self->input_buf); 
              std::string response (
                  std::istreambuf_iterator<char>(input_stream),{});

              // server logic handled here
              self->sendMessage(self->payload.exec(response));
          });
    }




    ip::tcp::socket socket;
    streambuf input_buf;
    server_commands payload;
};







