// TCP server (accepts connections & boots sessions)
// shamelessly stolen from:
// [here](https://dens.website/tutorials/cpp-asio/async-tcp-server)

#include <boost/asio.hpp>
#include "session.cpp"

using namespace boost::asio;


class server
{
public:

    server(io_context& ctx_, std::uint16_t port)
    : ctx(ctx_)
    , acceptor  (ctx, ip::tcp::endpoint(ip::tcp::v4(), port))
    {
    }

    // listen to incoming connections
    // upon a link init a session, pass it a new socket
    void listen()
    {
        socket.emplace(ctx);

        acceptor.async_accept(*socket, [&] (boost::system::error_code error)
        {
            std::make_shared<session>(std::move(*socket))->start();
            listen();
        });
    }

private:

    io_context& ctx;
    ip::tcp::acceptor acceptor;
    std::optional<ip::tcp::socket> socket;
};


