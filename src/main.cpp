/* remote calculator

build: `cd bin && cmake .. && make`
start: `./mathserver <port>` *dynamic port 1025-65535*
access: `netcat 127.0.0.1 <port>`
 
+ TCP in/out, multi-suer
- timeouts
+ login, password, calc, logout
+ acounts w/ points  (see userbase.cpp::init_dummy_userbase())
- user/logging database
+ basic arithmetics on floating-point numbers
- parenthesis
- adequate parser
- build scripts


*/

#include <boost/asio.hpp>
#include <cstdlib>
#include "server.cpp"



int main(int argc, char* argv[])
{
    long port = 0;
    if ((argc < 2) || ((port = strtol(argv[1],NULL,10)) < 1024))
        return -1;

    boost::asio::io_context io_context;
    server srv(io_context, port);
    srv.listen();
    io_context.run();
    return 0;
}



