// userbase: simple class to track registered users

#include <string>
#include <vector>
#include <fstream>

// action log written here
#define DEFAULT_LOG_FNAME "calc.log"


typedef unsigned long int user_id_type;
#define NULL_USER 0
#define USERPAD 100

// as return type of std::hash
// which is used to hash passwords
typedef size_t passwd_type;



struct user
{
    user_id_type id;
    std::string login;
    passwd_type pass_hash;
    unsigned int points;
};



class userbase
{
public:

    userbase()
    {
        log.open(DEFAULT_LOG_FNAME, std::ios::app);
    }

    ~userbase()
    {
        log.close();
    }

    user_id_type add_user (std::string new_login,
                           passwd_type new_pass_hash,
                           unsigned int new_points)
    {
        if (new_login.empty())
            return NULL_USER;
        for (user u: users)
            if (u.login.compare(new_login) == 0)
                return NULL_USER;

        users.push_back({.id        = users.size() + USERPAD,
                         .login     = new_login,
                         .pass_hash = new_pass_hash,
                         .points    = new_points});
        return users.back().id;
    }

    void drop_user(user_id_type id_to_drop)
    {
        if (id_to_drop - USERPAD < users.size())
            users.erase(users.begin()+(id_to_drop - USERPAD));
    }


    user_id_type exists (std::string username)
    {
        int foo;
        for (user u: users)
            if (u.login.compare(username) == 0)
                return u.id;
        // username not found
        return NULL_USER;
    }


    bool pass_correct (user_id_type id_to_auth,
                       passwd_type pass_hash_to_check)
    {
        // relies heavily on lazy bool evaluation
        // and operation order
        return (id_to_auth - USERPAD < users.size())
                && (users[id_to_auth - USERPAD]
                   .pass_hash == pass_hash_to_check);
    }

    bool action_allowed (user_id_type id_to_act)
    {
        // relies heavily on lazy bool evaluation
        // and operation order
        return (id_to_act - USERPAD < users.size())
                && (users[id_to_act - USERPAD].points > 0);
    }
    


    void register_request (user_id_type id_to_act,
                           const std::string& request,
                           const std::string& response)
    {
        // errors are silently ignored
        if (id_to_act - USERPAD >= users.size())
            return;

        --(users[id_to_act - USERPAD].points);

        if (log.is_open())
            log << users[id_to_act - USERPAD].login
                << "|ask|" << request
                << "|ans|" << response << std::endl;
    }



private:
    std::vector<user> users;
    std::ofstream log;
};




// for testing
#include <functional>
using std::hash;

// for testing purposes
// prior to this the base is expected to be empty
void init_dummy_userbase (userbase& base)
{
    std::hash<std::string> hasher;
    base.add_user("alice", hasher("ecila"), 10);
    base.add_user("bob", hasher("bob"), 10000);
    base.add_user("carl", hasher("lrac"), 0);
}













