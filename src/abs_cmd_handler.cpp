// take commands, launch related actions

#include <string>

#ifndef ABC_CMD_HANDLER
#define ABC_CMD_HANDLER

class abstract_command_handler
{
public:

    std::string exec(std::string command)
    {
        return "dummy command handler";
    }

private:

};

typedef abstract_command_handler cmd_handler;

#endif  // ABC_CMD_HANDLER


