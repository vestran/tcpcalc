// arithmetic operations over floating-point numbers
// TODO: parenthesis, automaton parser, ast, arg/op stack

#include <string>
#include <sstream>
#include <vector>

#include "abs_cmd_handler.cpp"



class float_calc 
    : public abstract_command_handler
{
public:

    std::string exec(std::string input_text)
    {
        // cleanup old calcualtion materials
        args.clear();
        ops.clear();
        if (input_text.compare("life universe and everything") == 0)
            return "42";
        // fill args and ops from given reqest
        std::string parse_err(parse(input_text));
        if (!parse_err.empty())
            return parse_err;
        else
            // do arithmetics
            return calculate();
    }


private:
    // take a string of floating-point numbers (arguments)
    // separated by arithmetic operators (+*-/) and whitespaces
    // from it fill args and ops
    std::string parse(std::string expr_text)
    {
        std::stringstream expr(expr_text,std::ios_base::in);
        // which part of string is being parsed;
        // only needed for error messages
        unsigned int expr_pos = 0; 
        // get 1st argument
        if (!get_arg(expr))
            return "Can't read number from position "
                   + std::to_string(expr_pos);
        else
            expr_pos = expr.tellg();

        // take an operator, then its second argument
        while (!expr.eof())
        {
            eat_spaces(expr);
            if (!get_op(expr))
                return "Can't read operation from position "
                       + std::to_string(expr_pos);
            else
                expr_pos = expr.tellg();

            if (!get_arg(expr))
                return "Can't read number from position "
                       + std::to_string(expr_pos);
            else
                expr_pos = expr.tellg();
            eat_spaces(expr);
        }
        return "";
    }


    // find a floating-point number in a stream
    // and put it in args
    bool get_arg(std::stringstream& src)
    {
        float num_now;
        if ((src >> num_now).fail())
            return false;
        //std::cout << num_now << '\n';
        args.push_back(num_now);
        return true;
    }


    // extract op: skip wspace, seek +/-*,
    // push it to ops. any other character is an error
    bool get_op(std::stringstream& src)
    {
        char op_now;
        //std::cout << "read op_now" << '\n';
        do {
            src.get(op_now);
            if (isspace(op_now))
                continue;
            if (   op_now != '+' && op_now != '-'
                && op_now != '*' && op_now != '/')
                return false;
            ops.push_back(op_now);
            break;
        } while(true);
        //std::cout << op_now << '\n';
        return true;
    }

    // take leading whitespaces from a stream
    void eat_spaces(std::stringstream& src)
    {
        int next = src.peek();
        while (isspace(next))
        {
            src.get();
            next = src.peek();
        }
    }

    // perform arithmetics
    // do i-th operation on numbers i and i+1
    // args: 4 2 3   // 4 + 2 * 3
    //  ops: + *
    // performed op and arg2 get consumed
    std::string calculate()
    {
        if (args.empty() || args.size() != ops.size() + 1 )
            return "Calculation error: arguments lost";

        size_t i = 0,lim = ops.size();
        // * and /
        for (i = 0; i < lim; i++)
            if (ops[i] == '*' || ops[i] == '/')
            {
                if ((ops[i] == '/') && (args[i + 1] == 0.0))
                    return "Calculation error: division by zero";
                args[i] = (ops[i] == '*') ? args[i] * args[i+1]
                                          : args[i] / args[i+1];

                 ops.erase( ops.begin() + i);
                args.erase(args.begin() + i + 1);
                lim--;
            }

        // + and -
        for (i = 0; i < lim; i++)
            if (ops[i] == '+' || ops[i] == '-')
            {
                args[i] = (ops[i] == '+') ? args[i] + args[i+1]
                                          : args[i] - args[i+1];

                 ops.erase( ops.begin() + i);
                args.erase(args.begin() + i + 1);
            }

        // 0 ops and 1 arg should be left
        return std::to_string(args.front());
        }

    std::vector<float> args;
    std::vector<char>  ops;
};

/*

// for standalone testing
int main ()
{
    float_calc abacus;
    //float foo = 8;
    //std::cin >> foo;
    std::cout << abacus.exec("123.0+4.5    * 43.21e0 abc") << '\n';


    return 0;
}
*/


