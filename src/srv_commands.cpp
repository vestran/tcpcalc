// interaction flow controller
// expected behavior is: login -> password -> most actions


// text length of longest action ("password")
#define ACTION_LONGEST 8



#include <string>
#include <unordered_map>

#include <functional>
using std::hash;

#include "abs_cmd_handler.cpp"
#include "userbase.cpp"
#include "fcalc.cpp"


class server_commands 
    : public abstract_command_handler
{

public:

    server_commands ()
        : command_set   // text substring to internal commands
        ({
            {"login", login},
            {"password", password},
            {"calc", calc},
            {"logout", logout}
        })
    {
        // fill user "database" with some test data
        init_dummy_userbase(base);
    }


    std::string exec(std::string input_text)
    {
        return add_prompt(process_input(input_text));
    }


private:

    enum command {unknown = 0, login, password, calc, logout};
    typedef std::unordered_map <std::string,enum command> str_to_cmd;


    // TODO: mb decompose
    std::string process_input(std::string input)
    {
        if (input.empty())
            return "No command";

        if (input.back() == '\n')
            input.pop_back();

        // break incoming text into "command" and "argument"
        // before and after first space, respectively
        // argument is optional
        size_t cmd_len = input.find(' ');
        if (cmd_len == std::string::npos)
            cmd_len = input.size();

        if (cmd_len > ACTION_LONGEST)
        {
            if (state == passwd_expected)
                state = login_expected;
            // other states handle wrong commands unchanged

            return "Command too long to be valid";
        }

        std::string cmd_text(input,0,cmd_len);
        std::string argument("");
        if (cmd_len + 1 < input.length())
            argument = input.substr(cmd_len+1); 

        // see if given command text belongs to the set
        str_to_cmd::const_iterator cmd_found
            = command_set.find(cmd_text);
        enum command cmd_now = (cmd_found == command_set.cend())
                               ? unknown
                               : (*cmd_found).second;
        if (!cmd_now)
        {
            if (state == passwd_expected)
                state = login_expected;
            // other states handle wrong commands unchanged

            return "Unknown command";// + cmd_text;
        }

        // TODO: proper state machine
        switch (state)
        {
        case login_expected: 
            return wait_login (cmd_now, argument); break;
        case passwd_expected:
            return wait_passwd(cmd_now, argument); break;
        case auth_done:
            return work_w_user(cmd_now, argument); break;
        //default: unreachable. assertion?
        }

        // unreachable. assertion?
        return "server_commands::exec() end";
    }



    std::string add_prompt(std::string text)
    {
       return   text + '\n' + logged_username + '>';
    }


    std::string wait_login (enum command cmd, std::string& username)
    {
        if (cmd != login)
            return "Log in first";
        else if (!base.exists(username))
             return "No such login";
        else
        {
            logged_user_id = base.exists(username);
            logged_username = username;
	    state = passwd_expected;
	    return "Enter password"; // " for " + username;
        }
    }


    std::string wait_passwd (enum command cmd, std::string& pass_text)
    {
        if (cmd != password)
        {
            logged_user_id = NULL_USER;
            logged_username = "";
            state = login_expected;
            return "Not a password";
        }
        else
        {
            std::hash<std::string> hasher;// hash pass
            if (!base.pass_correct(logged_user_id, hasher(pass_text)))
                return "Wrong password";
            else
            {
                state = auth_done;
                return "Login successful";
            }
        }
    }


    // when user is logged in, enable most commands
    std::string work_w_user (enum command cmd, std::string& arg)
    {
        switch (cmd)
        {
            case login: return wait_login(cmd, arg); break;
            case logout:
                logged_user_id = NULL_USER;
                logged_username = "";
                state = login_expected;
                return "Logged out";
                break;
            case password: // mb allow user to change password
                return "Already logged in";
                break;
            case calc: 
                if (!base.action_allowed(logged_user_id))
                    return "Action forbidden (check balance?)";
                std::string res(calculator.exec(arg));
                base.register_request(logged_user_id, arg, res);
                return res;
                break;
            //default: unreachable. assertion?
        }

        // unreachable. assertion?
        return "server_commands::work_w_user() end";
    }


    enum {login_expected, passwd_expected, auth_done} state;
    str_to_cmd command_set;

    userbase base;
    // data on a user trying to log in
    std::string logged_username;
    user_id_type logged_user_id;

    // math performer
    float_calc calculator;
};




